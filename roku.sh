#! /usr/bin/bash

if [ -z $3 ] && [ "$2" = "query" ]; then
	echo "Please provide a command and/or action to do"
	exit 1
fi

case $1 in
	girls)
		set -- '192.168.1.170' ${@:2}
		;;
	carson)
		set -- '192.168.1.138' ${@:2}
		;;
	master)
		set -- '192.168.1.227' ${@:2}
		;;
esac

if [ ! "$2" = "query" ]; then
	set -- "$1" 'keypress' "$2"
fi

echo "${@}"

if [ "$2" = "keypress" ] || [ "$2" = "keydown" ] || [ "$2" = "keyup" ]; then
	curl -sSd '' "http://${1}:8060/${2}/${3}"
else
	curl -sS "http://${1}:8060/${2}/${3}"
fi
